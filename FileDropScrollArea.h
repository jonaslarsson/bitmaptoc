/* BitmapToC
 * Copyright (C) 2013 Jonas Larsson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#ifndef FILEDROPSCROLLAREA_H
#define FILEDROPSCROLLAREA_H

#include <QScrollArea>

class FileDropScrollArea : public QScrollArea
{
    Q_OBJECT
public:
    explicit FileDropScrollArea(QWidget *parent = 0);

signals:
    void loadImage(QString filename);

protected:
    void dragEnterEvent(QDragEnterEvent* event);
    void dropEvent(QDropEvent* event);
};

#endif // FILEDROPSCROLLAREA_H
