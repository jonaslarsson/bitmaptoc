#-------------------------------------------------
#
# Project created by QtCreator 2013-04-06T15:46:23
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

win32:QMAKE_LFLAGS  += -static-libgcc -lstdc++ -static

TARGET = BitmapToC
TEMPLATE = app


SOURCES += main.cpp\
        MainWindow.cpp \
    FileDropScrollArea.cpp

HEADERS  += MainWindow.h \
    FileDropScrollArea.h \
    Version.h

FORMS    += MainWindow.ui

RC_FILE = BitmapToC.rc

OTHER_FILES += \
    BitmapToC.rc

static:CONFIG(release, debug|release): QMAKE_POST_LINK += $${_PRO_FILE_PWD_}/upx.exe -9 $${OUT_PWD}/release/$${TARGET}.exe
