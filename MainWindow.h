/* BitmapToC
 * Copyright (C) 2013 Jonas Larsson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QImage>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void onLoadImage(QString filename);
    void on_formatComboBox_currentIndexChanged(int index);

private:
    void redrawImage();
    void generateSourceCode();
    Ui::MainWindow *m_gui;
    QImage m_originalImage;
    enum ImageFormat {
        BlackWhite_1bit = 0,
        Greyscale_8bit
    } m_imageFormat;
};

#endif // MAINWINDOW_H
