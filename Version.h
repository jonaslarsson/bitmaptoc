/* BitmapToC
 * Copyright (C) 2013 Jonas Larsson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             0,1,0,0
#define VER_FILEVERSION_STR         "0.1\0"

#define VER_PRODUCTVERSION          0,1,0,0
#define VER_PRODUCTVERSION_STR      "0.1\0"

#define VER_COMPANYNAME_STR         "System Refine"
#define VER_FILEDESCRIPTION_STR     "BitmapToC"
#define VER_INTERNALNAME_STR        "BitmapToC"
#define VER_LEGALCOPYRIGHT_STR      "Copyright � 2013 Jonas Larsson"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "BitmapToC.exe"
#define VER_PRODUCTNAME_STR         "BitmapToC"

#endif // VERSION_H
