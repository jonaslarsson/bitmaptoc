/* BitmapToC
 * Copyright (C) 2013 Jonas Larsson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#include <QDebug>
#include <QImage>
#include "Version.h"
#include "MainWindow.h"
#include "ui_MainWindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_gui(new Ui::MainWindow)
{
    m_gui->setupUi(this);
    connect(m_gui->scrollArea, SIGNAL(loadImage(QString)), this, SLOT(onLoadImage(QString)));
    m_imageFormat = (ImageFormat)m_gui->formatComboBox->currentIndex();
    this->setWindowTitle(this->windowTitle() + " "VER_FILEVERSION_STR);
}

MainWindow::~MainWindow()
{
    delete m_gui;
}

void MainWindow::onLoadImage(QString filename)
{
    m_originalImage.load(filename);
    redrawImage();
}

void MainWindow::on_formatComboBox_currentIndexChanged(int index)
{
    m_imageFormat = (ImageFormat)index;
    redrawImage();
}

static QImage imageToGrayscale(QImage image)
{
    for (int i = 0; i < image.width(); i++)
    {
        for (int j = 0; j < image.height(); j++)
        {
            QRgb col = image.pixel(i, j);
            int gray = qGray(col);
            image.setPixel(i, j, qRgb(gray, gray, gray));
        }
    }
    return image;
}

void MainWindow::redrawImage()
{
    QImage image;
    switch (m_imageFormat)
    {
    case BlackWhite_1bit:
        image = m_originalImage.convertToFormat(QImage::Format_Mono, Qt::MonoOnly | Qt::ThresholdDither);
        break;
    case Greyscale_8bit:
        image = imageToGrayscale(m_originalImage);
        break;
    }

    m_gui->imageLabel->setPixmap(QPixmap::fromImage(image));
    m_gui->imageLabel->setMaximumWidth(m_originalImage.width());
    m_gui->imageLabel->setMaximumHeight(m_originalImage.height());
    m_gui->imageLabel->setMinimumWidth(m_originalImage.width());
    m_gui->imageLabel->setMinimumHeight(m_originalImage.height());

    generateSourceCode();
}

void MainWindow::generateSourceCode()
{
    QByteArray imageData;
    QImage image = m_gui->imageLabel->pixmap()->toImage();
    switch (m_imageFormat)
    {
    case BlackWhite_1bit:
        image = m_originalImage.convertToFormat(QImage::Format_Mono, Qt::MonoOnly | Qt::ThresholdDither);
        imageData.setRawData((const char *)image.bits(), image.byteCount());
        break;
    case Greyscale_8bit:
    {
        image = m_originalImage.convertToFormat(QImage::Format_RGB888);
        for (int i = 0; i < image.width(); i++)
        {
            for (int j = 0; j < image.height(); j++)
            {
                QRgb col = image.pixel(i, j);
                int gray = qGray(col);
                imageData.append((const char)gray);
            }
        }
        break;
    }
    }

    QString source;
    source.append("const unsigned int ImageWidth = " + QString::number(image.width()) + ";\n");
    source.append("const unsigned int ImageHeight = " + QString::number(image.height()) + ";\n");
    source.append("const unsigned int ImageDataSize = " + QString::number(imageData.size()) + ";\n");
    source.append("const unsigned char *ImageData = {\n");
    int count = 0;
    for (int i = 0; i < imageData.size(); i++)
    {
        QString hexByte;
        hexByte.sprintf("0x%02x", (quint8)imageData.at(i));

        if (count > 0)
        {
            source.append(", ");
        }
        source.append(hexByte);
        if (count == 15)
        {
            count = 0;
            if (i < imageData.size() - 1)
            {
                source.append(",");
            }
            source.append("\n");
        } else {
            count++;
        }
    }
    source.append("};\n");
    m_gui->plainTextEdit->setPlainText(source);
}
