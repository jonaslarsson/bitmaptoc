/* BitmapToC
 * Copyright (C) 2013 Jonas Larsson
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 */
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include <QUrl>
#include <QFile>
#include <QFileInfo>
#include <QImageReader>
#include "FileDropScrollArea.h"

FileDropScrollArea::FileDropScrollArea(QWidget *parent) :
    QScrollArea(parent)
{
}

void FileDropScrollArea::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls())
    {
        QList<QUrl> urls = event->mimeData()->urls();
        if (urls.size() == 1)
        {
            QString filename = urls.at(0).toLocalFile();
            QFileInfo fileinfo(filename);
            if (fileinfo.exists() && QImageReader::supportedImageFormats().contains(fileinfo.suffix().toLatin1()))
            {
                // Only images that both exists and which we support
                event->accept();
            }
        }
    }
}

void FileDropScrollArea::dropEvent(QDropEvent *event)
{
    if (event->mimeData()->hasUrls())
    {
        QList<QUrl> urls = event->mimeData()->urls();
        if (urls.size() == 1)
        {
            QString filename = urls.at(0).toLocalFile();
            emit loadImage(filename);
        }
    }
}
